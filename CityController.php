<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Content;
use App\Models\ContentReaded;
use App\Models\Detail;
use App\Models\Product;
use App\Models\Type;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use function PHPUnit\Framework\fileExists;

class CityController extends Controller
{
    public function __construct()
    {
        $this->middleware('api-token')->only('getAllCitiesByuser', 'getCityByCityNameByuser', 'store', 'edit', 'destroy'
            , 'getCityById', 'getAllCitiesByAdmin', 'getCityByCityNameByAdmin');
        $this->middleware('auth:sanctum')->only( 'index','create', 'update', 'delete');


    }


    protected $view = 'admin.cities.';

    public function index()
    {
        $cities = City::select('id', 'name', 'imageUrl')->get();
        return view($this->view . 'index', compact('cities'));
    }


    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',

            'imageUrl' => 'required|image|mimes:jpg,png,jpeg',
        ], [], []);


        $imageName = 'city_' . uniqid() . '.' . $request->imageUrl->getClientOriginalExtension();
        $imagePath = '/uploads/' . $imageName;
        $request->imageUrl->move(public_path('uploads'), $imageName);

        $city = City::create([
            'name' => $request->name,
            'imageUrl' => $imagePath
        ]);


        return redirect()->route('cityIndex')->withMessage('City added successfully');
    }


    public function update(Request $request, City $city)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'imageUrl' => 'sometimes',
        ], [], []);

        $city->update($request->except(['imageUrl']));

        if ($request->imageUrl) {
            if ($city->imageUrl) {
                if (fileExists(public_path() . $city->imageUrl)) {
                    File::delete(public_path() . $city->imageUrl);
                }

                $imageName = 'city_' . uniqid() . '.' . $request->imageUrl->getClientOriginalExtension();
                $imagePath = '/uploads/' . $imageName;
                $request->imageUrl->move(public_path('uploads'), $imageName);
                $city->update(['imageUrl' => $imagePath]);

            } else {
                $imageName = 'city_' . uniqid() . '.' . $request->imageUrl->getClientOriginalExtension();
                $imagePath = '/uploads/' . $imageName;
                $request->imageUrl->move(public_path('uploads'), $imageName);
                $city->update(['imageUrl' => $imagePath]);
            }
        }


        return redirect()->route('cityIndex')->withMessage('City updated successfully');
    }


    public function delete(City $city)
    {
        if (!$city) {
            return abort('404');
        }
        if ($city->types()->cursor()->count()) {
            foreach ($city->types()->cursor() as $type) {
                foreach ($type->contents()->cursor() as $content) {
                    File::delete(public_path() . $content->imageUrl);
                    $detail = Detail::query()->where('content_id', $content->id)->first();
                    if ($content->detail()) {
                        if (fileExists(public_path() . $content->detail->imageUrlLocation)) {
                            File::delete(public_path() . $content->detail->imageUrlLocation);
                        }
                        if (fileExists(public_path() . $content->detail->imageUrl1)) {
                            File::delete(public_path() . $content->detail->imageUrl1);
                        }
                        if (fileExists(public_path() . $content->detail->imageUrl2)) {
                            File::delete(public_path() . $content->detail->imageUrl2);
                        }
                        if (fileExists(public_path() . $content->detail->imageUrl3)) {
                            File::delete(public_path() . $content->detail->imageUrl3);
                        }
                        $content->detail()->delete();
                        $content->delete();
                    }

                }

            }
        }
        File::delete(public_path() . $city->imageUrl);
        $city->delete();
        return redirect('/city')->withMessage('City deleted successfully');
    }


    ////////////////////API//////////////////
    public function store(Request $request)
    {


        $imageName = 'city_' . time() . '.' . $request->cityImage->getClientOriginalExtension();
        $imagePath = '/uploads/' . $imageName;
        $request->cityImage->move(public_path('uploads'), $imageName);

        $city = City::create([
            'name' => $request->cityName,
            'imageUrl' => $imagePath
        ]);
        $city->setAttribute('cityName', $city->name);
        $city->setAttribute('cityImage', $city->imageUrl);
        return response()->json($city->makeHidden(['name', 'imageUrl', 'updated_at', 'created_at']));
    }

    public function edit(Request $request)
    {
        $city = City::query()->where('id', $request->id)->first();
        if (!$city) return response()->json('no city for this id');


        $city->update(['name' => $request->cityName != null ? $request->cityName : $city->name]);
        if ($request->cityImage) {
            if ($city->imageUrl)
//            {
                if (fileExists(public_path() . $city->imageUrl)) {
                    File::delete(public_path() . $city->imageUrl);
                }

            $imageName = 'city_' . time() . '.' . $request->cityImage->getClientOriginalExtension();
            $imagePath = '/uploads/' . $imageName;
            $request->cityImage->move(public_path('uploads'), $imageName);
            $city->update(['imageUrl' => $imagePath]);

        }
        $city->setAttribute('cityName', $city->name);
        $city->setAttribute('cityImage', $city->imageUrl);
        return response()->json($city->makeHidden(['name', 'imageUrl', 'updated_at', 'created_at']));
    }

    public function destroy($id)
    {
        $city = City::query()->where('id', $id)->first();
        if (!$city) {
            return response()->json('no city for this id');
        }
        if ($city->types()->cursor()->count()) {
            foreach ($city->types()->cursor() as $type) {
                foreach ($type->contents()->cursor() as $content) {
                    File::delete(public_path() . $content->imageUrl);
                    $detail = Detail::query()->where('content_id', $content->id)->first();
                    File::delete(public_path() . $detail->imageUrl1);
                    File::delete(public_path() . $detail->imageUrl2);
                    File::delete(public_path() . $detail->imageUrl3);
                    File::delete(public_path() . $detail->imageUrlLocation);
                    $content->detail()->delete();
                }
                $type->contents()->delete();
                File::delete(public_path() . $type->imageUrl);
            }
        }
        File::delete(public_path() . $city->imageUrl);
        $city->delete();
        return response()->json(['city' => $city]);
    }

    public function getAllCities(Request $request)
    {
        $cities = City::get(['id', 'name', 'imageUrl']);
        $cities->map(function ($city) use ($request) {
            $city['cityName'] = $city->name;
            $city['cityImage'] = $city->imageUrl;
            $city['contentoffer'] = $this->getContentissubscribe($city->id, $request);
            unset($city->name, $city->imageUrl);
            return $city;
        });
        return response()->json($cities);
    }

    public function getAllCitiesByuser()
    {
        $cities = City::get(['id', 'name', 'imageUrl']);
        $cities->map(function ($city) {
            $city['cityName'] = $city->name;
            $city['cityImage'] = $city->imageUrl;
            $city['contentoffer'] = $this->getcountcontenthasoffer($city->id);
            unset($city->name, $city->imageUrl);
            return $city;
        });
        return response()->json($cities);
    }


    public function getCityByCityName($city_name, Request $request)
    {
        $city = City::query()->select('id', 'name', 'imageUrl')->where('name', $city_name)->first();
        $city->setAttribute('cityName', $city->name);
        $city->setAttribute('cityImage', $city->imageUrl);
        $city['contentoffer'] = $this->getContentissubscribe($city->id, $request);
        return response()->json($city->makeHidden(['name', 'imageUrl', 'updated_at', 'created_at']));
    }

    public function getCityByCityNameByuser($city_name)
    {
        $city = City::query()->select('id', 'name', 'imageUrl')->where('name', $city_name)->first();
        $city->setAttribute('cityName', $city->name);
        $city->setAttribute('cityImage', $city->imageUrl);
        $city['contentoffer'] = $this->getcountcontenthasoffer($city->id);
        return response()->json($city->makeHidden(['name', 'imageUrl', 'updated_at', 'created_at']));
    }

    public function getCityByCityNameByAdmin($city_name)
    {
        $city = City::query()->select('id', 'name', 'imageUrl')->where('name', $city_name)->first();
        $city->setAttribute('cityName', $city->name);
        $city->setAttribute('cityImage', $city->imageUrl);
        $city['contentoffer'] = $this->getcountcontenthasoffer($city->id);
        return response()->json($city->makeHidden(['name', 'imageUrl', 'updated_at', 'created_at']));
    }

    public function getCityById($city_id)
    {
        $city = City::query()->select('id', 'name', 'imageUrl')->findOrFail($city_id);
        $city->setAttribute('cityName', $city->name);
        $city->setAttribute('cityImage', $city->imageUrl);
        $city['contentoffer'] = $this->getcountcontenthasoffer($city->id);////عدد الكونتت التى تحوي عروض///
        return response()->json($city->makeHidden(['name', 'imageUrl', 'updated_at', 'created_at']));
    }

    public function getcountcontenthasoffer($city_id)
    {

        if ($city_id == 0) {

            $contents = Content::query()->with('type')->whereNotIn('count_offer', [0])->get()
                ->count();

            return $contents;
        } else {
            $contents = Content::query()->with('type')->whereNotIn('count_offer', [0])->get()
                ->filter(function ($item) use ($city_id) {
                    return $item['type']['city_id'] == $city_id;
                })->count();

            return $contents;
        }
    }

}
