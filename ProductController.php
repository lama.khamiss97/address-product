<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Content;
use App\Models\ContentReaded;
use App\Models\Detail;
use App\Models\ImageEstate;
use App\Models\NotificationFcm;
use App\Models\NotificationView;
use App\Models\Product;
use App\Models\StoryView;
use App\Models\User;
use App\Repository\Notification;
use App\Traits\ContentTrait;
use App\Traits\CreateNotificationTrait;
use App\Traits\OfferTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\False_;
use PhpParser\Node\Stmt\DeclareDeclare;
use Symfony\Component\Console\Input\Input;
use function PHPUnit\Framework\fileExists;
use function Sodium\compare;

class ProductController extends Controller
{
    use OfferTrait, ContentTrait, CreateNotificationTrait;


    public function __construct()
    {
        $this->offerIsExpired();
        $this->checkContentsExpired();
        $this->middleware('api-token')->only('getAllProductsByCategoryId',
            'getAllProductsByContentId', 'getAllProductsisOfferByContentId'
            , 'getSearchProductsByContentIdAndIdcategory');


    }


    public function index($offer, $cat_id = null, Request $request)
    {


        $content = Content::query()->where('subscriber_id', auth()->id())->first();
        $detail = Detail::query()->where('content_id', $content->id)->first();
        $cats = Category::query()->where('content_id', $content->id)->get();
        $cat = null;
        if ($cat_id != null && $offer == 'Category') {
            $cat = Category::query()->findOrFail($cat_id);
            $products = Product::query()->where('category_id', $cat->id)->with('category')->get();
        } elseif ($cat_id == null && $offer == 'offer') {
            $products = Product::query()->with('category')
                ->whereHas('category', function ($query) use ($content) {
                    $query->where('content_id', $content->id);
                })
                ->where('is_offer', true)->get();
        } elseif ($cat_id == null && $offer == 'Home') {
            $products = Product::query()->with('category')
                ->whereHas('category', function ($query) use ($content) {
                    $query->where('content_id', $content->id);
                })->get();
        }
        return view('subscribers.mainProducts', compact('products', 'cat', 'cats', 'cat_id', 'content', 'detail'));
    }

    public function subscriberFront($content_id, $offer, $cat_id = null)
    {


        $content = Content::query()->with('SubscriberUser')->where('id', $content_id)->first();
        $detail = Detail::query()->where('content_id', $content->id)->first();
        $cats = Category::query()->where('content_id', $content->id)->get();
        $cat = null;
        if ($cat_id != null && $offer == 'Category') {
            $cat = Category::query()->findOrFail($cat_id);
            $products = Product::query()->where('category_id', $cat->id)->with('category')->get();
        } elseif ($cat_id == null && $offer == 'offer') {
            $products = Product::query()->with('category')
                ->whereHas('category', function ($query) use ($content) {
                    $query->where('content_id', $content->id);
                })->where('is_offer', true)->get();
        } elseif ($cat_id == null && $offer == 'Home') {
            $products = Product::query()->with('category')
                ->whereHas('category', function ($query) use ($content) {
                    $query->where('content_id', $content->id);
                })->get();
        }
        return view('frontend.SubscribersProducts', compact('products', 'cat', 'cats', 'cat_id', 'content', 'detail'));
    }

    public function productByCat($cat_id)
    {
        $cat = Category::query()->findOrFail($cat_id);
        $content = Content::query()->findOrFail($cat->content_id);
        $detail = Detail::query()->where('content_id', $content->id)->first();
        $cats = Category::query()->where('content_id', $cat->content_id)->get();
        if ($cat_id != null) {
            $products = Product::query()->where('category_id', $cat->id)->get();
        } else $products = Product::query()->get();
        if ($cats->isEmpty()) $cats = Category::query()->get();
        return view('frontend.SubscribersProducts', compact('products', 'cats', 'cat_id', 'content', 'detail'));
    }

    public function productDetailsFront($id)
    {
        $product = Product::query()->where('id', $id)->with('category')->first();
        $cat = Category::query()->where('id', $product->category_id)->first();
        $relatedProduct = Product::query()->where('category_id', $cat->id)->whereKeyNot($product->id)
            ->get()->sortBy('created_at')->take(4);
        $content = Content::query()->where('id', $cat->content_id)->first();
        $cats = Category::query()->where('content_id', $content->id)->get();
        $detail = Detail::query()->where('content_id', $content->id)->first();

        return view('frontend.SubscribersProductsdetails', compact('product', 'cat', 'relatedProduct', 'content', 'cats', 'detail'));
    }


    public function getProductByCat($cat_id)
    {
        $content = Content::query()->where('subscriber_id', auth()->id())->first();
        $cats = Category::query()->where('content_id', $content->id)->get();
        if ($cat_id != null) {
            $cat = Category::query()->findOrFail($cat_id);
            $products = Product::query()->where('category_id', $cat->id)->get();
        } else $products = Product::query()->get();
        if ($cats->isEmpty()) $cats = Category::query()->get();
        return view('subscribers.products', compact('products', 'cats', 'cat_id'));
    }

    public function create(Request $request)
    {


        $this->validate($request, [
            'name' => 'required',
        ], [], []);
        return DB::transaction(function () use ($request) {
            $data = $request->except(['image_product', 'image1', 'image2', 'image3', 'image4', 'image5']);
            if ($request->product_image_main) {

                $data['image_product'] = uploadImageBase64($request->product_image_main, 'product');
            }
            if ($request->product_image1) {
                $data['image1'] = uploadImageBase64($request->product_image1, 'product');

            }
            if ($request->product_image2) {
                $data['image2'] = uploadImageBase64($request->product_image2, 'product');

            }
            if ($request->product_image3) {
                $data['image3'] = uploadImageBase64($request->product_image3, 'product');
            }
            if ($request->product_image4) {
                $data['image4'] = uploadImageBase64($request->product_image4, 'product');

            }

            $cat = Category::query()->findOrFail($request->cat);
            $data['category_id'] = $cat->id;

            $product = Product::create($data);

            $sys_log_product = DB::table('system_log_products')
                ->insert(['product_id' => $product->id, 'user_id' => auth()->id(),
                    'description' => $product,'operation'=>'create']);
            return redirect('subscriber/product/Category/' . $request->cat);

        });

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {


        return DB::transaction(function () use ($request) {
            $product = Product::query()->findOrFail($request->product_id);

            $imagePathProduct = null;
            if ($request->edit_image_main) {
                if ($product->image_product)
                    if (fileExists(public_path() . $product->image_product)) {
                        File::delete(public_path() . $product->image_product);
                    }
                $imagePathProduct = uploadImageBase64($request->edit_image_main, 'product');

                $product->image_product = $imagePathProduct;

            }
            $imagePath1 = null;
            if ($request->edit_image1) {
                if ($product->image1)
                    if (fileExists(public_path() . $product->image1)) {
                        File::delete(public_path() . $product->image1);
                    }
                $imagePath1 = uploadImageBase64($request->edit_image1, 'product');

                $product->image1 = $imagePath1;
            }
            $imagePath2 = null;
            if ($request->edit_image2) {
                if ($product->image2)
                    if (fileExists(public_path() . $product->image2)) {
                        File::delete(public_path() . $product->image2);
                    }

                $imagePath2 = uploadImageBase64($request->edit_image2, 'product');


                $product->image2 = $imagePath2;
            }
            $imagePath3 = null;
            if ($request->edit_image3) {

                if ($product->image3)
                    if (fileExists(public_path() . $product->image3)) {
                        File::delete(public_path() . $product->image3);
                    }
                $imagePath3 = uploadImageBase64($request->edit_image3, 'product');


                $product->image3 = $imagePath3;
            }

            $imagePath4 = null;
            if ($request->edit_image4) {

                if ($product->image4)
                    if (fileExists(public_path() . $product->image4)) {
                        File::delete(public_path() . $product->image4);
                    }
                $imagePath4 = uploadImageBase64($request->edit_image4, 'product');

                $product->image4 = $imagePath4;
            }

            $product->name = $request->name;
            $product->description = $request->description;
            $product->price = $request->price;

            $product->save();
            $sys_log_product = DB::table('system_log_products')
                ->insert(['product_id' => $product->id, 'user_id' => auth()->id(),
                    'description' => $product,'operation'=>'update']);
            return redirect('subscriber/product/Category/' . $request->category_id);
        });

    }

    public function editOffer(Request $request)
    {
//        dd($request->all());


        $this->validate($request, [

            'sellist1' => 'required',
        ], [], []);
        return DB::transaction(function () use ($request) {
            $product = Product::query()->with('category.content.type.city')->findOrFail($request->id);

            $product->name = $request->name;
            $product->description = $request->description;
            $product->price = $request->price;
            $product->price_new = $request->price_new;
            $product->days_offer = $request->sellist1;
            $product->is_offer = true;
            $product->date_offer = Carbon::now()->addDays($request->sellist1);
            $product->save();
            $city_id = $product['category']['content']['type']['city']['id'];
            $content = Content::query()->where('id', $product['category']['content_id'])->first();
            $content->count_offer = 1;
            $content->save();
            $content_read = ContentReaded::query()->where('content_id', $content->id)->delete();
            $notification = new Notification();
            $body = 'عرض جديد من ' . $content->name;
            $title = 'AddressApp';
            $item_type = 'product';
            $createNotification = $this->createNotification($title, $body, $content['detail']['logo'], $product->id, $item_type);
            $notification->pushNotification($title, $body, $city_id, $product->id, $item_type, $content['detail']['logo'], $createNotification->id);

            $sys_log_product = DB::table('system_log_products')
                ->insert(['product_id' => $product->id, 'user_id' => auth()->id(),
                    'description' => $product,'operation'=>'editOffer']);
            return redirect('subscriber/product/Category/' . $request->category_id);

        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return DB::transaction(function () use ($id) {
            $product = Product::query()->with('category')->where('id', $id)->first();
            if (fileExists(public_path() . $product->image_product)) {
                File::delete(public_path() . $product->image_product);
            }
            if (fileExists(public_path() . $product->image1)) {
                File::delete(public_path() . $product->image1);
            }
            if (fileExists(public_path() . $product->image2)) {
                File::delete(public_path() . $product->image2);
            }
            if (fileExists(public_path() . $product->image3)) {
                File::delete(public_path() . $product->image3);
            }
            if (fileExists(public_path() . $product->image4)) {
                File::delete(public_path() . $product->image4);
            }
            if (fileExists(public_path() . $product->image5)) {
                File::delete(public_path() . $product->image5);
            }
            if ($product->is_offer == 1) {
                $product->is_offer = false;
                $product->days_offer = null;
                $product->date_offer = null;
                $product->price_new = null;
                $product->save();
            }
            $content = Content::query()->where('id', $product['category']['content_id'])->first();
            $product_offer = Product::query()->with('category')
                ->whereHas('category', function ($query) use ($content) {
                    $query->where('content_id', $content->id);
                })
                ->where('is_offer', true)
                ->get()->count();
            $notifications = NotificationFcm::query()->where(['item_id' => $product->id, 'item_type' => 'product'])->get();
            if ($notifications != null) {
                foreach ($notifications as $notification) {
                    $notifiction_like = NotificationView::query()->where('notification_id', $notification->id)->delete();

                    $notification->delete();
                }
            }

            if ($product_offer == 0) {
                $content->count_offer = 0;
                $content->save();
                $content_read = ContentReaded::query()->where('content_id', $content->id)->delete();

            }


            $product->delete();
            return redirect('subscriber/product/Category/' . $product->category_id);
        });
    }

    public function productDetails($id)
    {

        $product = Product::query()->where('id', $id)->with('category')->first();

        $cat = Category::query()->where('id', $product->category_id)->first();

        $relatedProduct = Product::query()->where('category_id', $cat->id)->whereKeyNot($product->id)
            ->get()->sortBy('created_at')->take(4);

        $content = Content::query()->where('id', $cat->content_id)->first();

        $cats = Category::query()->where('content_id', $content->id)->get();
        $detail = Detail::query()->where('content_id', $content->id)->first();

        return view('subscribers.details12', compact('product', 'cat', 'relatedProduct', 'content', 'cats', 'detail'));
    }


    public function OfferDelete($id)
    {
        return DB::transaction(function () use ($id) {
            $product = Product::query()->where('id', $id)->first();
            $product->is_offer = false;
            $product->days_offer = null;
            $product->date_offer = null;
            $product->price_new = null;
            $product->save();
            $content = Content::query()->where('id', $product['category']['content_id'])->first();
            $product_offer = Product::query()->with('category')
                ->whereHas('category', function ($query) use ($content) {
                    $query->where('content_id', $content->id);
                })
                ->where('is_offer', true)
                ->get()->count();
            $notifications = NotificationFcm::query()->where(['item_id' => $product->id, 'item_type' => 'product'])->get();
            if ($notifications != null) {
                foreach ($notifications as $notification) {
                    $notifiction_like = NotificationView::query()->where('notification_id', $notification->id)->delete();

                    $notification->delete();
                }
            }

            if ($product_offer == 0) {
                $content->count_offer = 0;
                $content->save();
                $content_read = ContentReaded::query()->where('content_id', $content->id)->delete();

            }


            return redirect('subscriber/product/offer');
        });
    }

    public
    function searchProduct(Request $request)
    {
        $Search = $request->query('Search_pro');
        $content = Content::query()->where('subscriber_id', auth()->id())->first();

        $detail = Detail::query()->where('content_id', $content->id)->first();

        $cats = Category::query()->where('content_id', $content->id)->get();
        $cat = null;
        $cat_id = null;
        $products = Product::query()->with('category')
            ->whereHas('category', function ($query) use ($content) {
                $query->where('content_id', $content->id);
            })->get();


        if ($Search != null) {

            $array_url = explode('/', $request->url_pro);
            if (isset($array_url[3])) $offer = $array_url[3];

            if (isset($array_url[4])) $cat_id = (integer)$array_url[4];

            if ($cat_id != null && $offer == 'Category') {
                $cat = Category::query()->findOrFail($cat_id);
                $products = Product::query()
                    ->where('name', 'like', '%' . $Search . '%')
                    ->where('category_id', $cat->id)
                    ->with('category')->get();
            } elseif ($cat_id == null && $offer == 'offer') {
                $products = Product::query()->where('name', 'like', '%' . $Search . '%')->with('category')
                    ->whereHas('category', function ($query) use ($content) {
                        $query->where('content_id', $content->id);
                    })
                    ->where('is_offer', true)->get();
            } elseif ($cat_id == null && $offer == 'Home') {
                $products = Product::query()->where('name', 'like', '%' . $Search . '%')->with('category')
                    ->whereHas('category', function ($query) use ($content) {
                        $query->where('content_id', $content->id);
                    })->get();

            }

            return view('subscribers.mainProducts', compact('products', 'cat', 'cats', 'cat_id', 'content', 'detail'));

        }
    }

    public
    function searchProductfront(Request $request)
    {
        $array_url = explode('/', $request->url_pro);

        if (isset($array_url[2])) {
            $content_id = (integer)$array_url[2];
            $Search = $request->query('Search_pro');
            $contentuser = Content::query()->where('id', $content_id)->first();
            $content = Content::query()->where('subscriber_id', $contentuser->subscriber_id)->first();
            $detail = Detail::query()->where('content_id', $content->id)->first();
            $cats = Category::query()->where('content_id', $content->id)->get();
            $cat = null;
            $cat_id = null;
            $products = Product::query()->with('category')
                ->whereHas('category', function ($query) use ($content) {
                    $query->where('content_id', $content->id);
                })->get();
            if ($Search != null) {


                if (isset($array_url[3])) $offer = $array_url[3];

                if (isset($array_url[4])) $cat_id = (integer)$array_url[4];

                if ($cat_id != null && $offer == 'Category') {
                    $cat = Category::query()->findOrFail($cat_id);
                    $products = Product::query()
                        ->where('name', 'like', '%' . $Search . '%')
                        ->where('category_id', $cat->id)
                        ->with('category')->get();
                } elseif ($cat_id == null && $offer == 'offer') {
                    $products = Product::query()->where('name', 'like', '%' . $Search . '%')->with('category')
                        ->whereHas('category', function ($query) use ($content) {
                            $query->where('content_id', $content->id);
                        })
                        ->where('is_offer', true)->get();
                } elseif ($cat_id == null && $offer == 'Home') {
                    $products = Product::query()->where('name', 'like', '%' . $Search . '%')->with('category')
                        ->whereHas('category', function ($query) use ($content) {
                            $query->where('content_id', $content->id);
                        })->get();

                }
            }
            return view('frontend.SubscribersProducts', compact('products', 'cat', 'cats', 'cat_id', 'content', 'detail'));

        }
    }

/////////////////////////Api////////
    public
    function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'category_id' => 'required',
        ], [], []);
        return DB::transaction(function () use ($request) {
            $data = $request->except(['image_product', 'image1', 'image2', 'image3', 'image4', 'image5']);
            if ($request->image_product) {
                $imageNameProduct = 'product_' . uniqid() . '.' . $request->image_product->getClientOriginalName();
                $data['image_product'] = '/uploads/' . $imageNameProduct;
                $request->image_product->move(public_path('uploads'), $imageNameProduct);
            }
            if ($request->image1) {
                $imageName1 = 'product_' . uniqid() . '.' . $request->image1->getClientOriginalName();
                $data['image1'] = '/uploads/' . $imageName1;
                $request->image1->move(public_path('uploads'), $imageName1);
            }
            if ($request->image2) {
                $imageName2 = 'product_' . uniqid() . '.' . $request->image2->getClientOriginalName();
                $data['image2'] = '/uploads/' . $imageName2;
                $request->image2->move(public_path('uploads'), $imageName2);
            }
            if ($request->image3) {
                $imageName3 = 'product_' . uniqid() . '.' . $request->image3->getClientOriginalName();
                $data['image3'] = '/uploads/' . $imageName3;
                $request->image3->move(public_path('uploads'), $imageName3);
            }
            if ($request->image4) {
                $imageName4 = 'product_' . uniqid() . '.' . $request->image4->getClientOriginalName();
                $data['image4'] = '/uploads/' . $imageName4;
                $request->image4->move(public_path('uploads'), $imageName4);
            }
            if ($request->image5) {
                $imageName5 = 'product_' . uniqid() . '.' . $request->image5->getClientOriginalName();
                $data['image5'] = '/uploads/' . $imageName5;
                $request->image5->move(public_path('uploads'), $imageName5);
            }

            $category = Category::query()->where('id', $request->category_id)->first();
            if ($category) {
                $data['category_id'] = $category->id;
                $product = Product::create($data);

                return response()->json(Product::query()->findOrFail($product->id));

            }
        });

    }

    public
    function update(Request $request, $id)
    {
        $product = Product::query()->findOrFail($id);

        return DB::transaction(function () use ($request, $product) {
            $data = $request->except(['image_product', 'image1', 'image2', 'image3', 'image4', 'image5']);
            if ($request->image_product) {
                if ($product->image_product)
                    if (fileExists(public_path() . $product->image_product)) {
                        File::delete(public_path() . $product->image_product);
                    }
                $imageNameProduct = 'product_' . uniqid() . '.' . $request->image_product->getClientOriginalName();
                $imagePathProduct = '/uploads/' . $imageNameProduct;
                $request->image_product->move(public_path('uploads'), $imageNameProduct);
                $product->image_product = $imagePathProduct;
            }
            if ($request->image1) {
                if ($product->image1)
                    if (fileExists(public_path() . $product->image1)) {
                        File::delete(public_path() . $product->image1);
                    }
                $imageName1 = 'product_' . uniqid() . '.' . $request->image1->getClientOriginalName();
                $imagePath1 = '/uploads/' . $imageName1;
                $request->image1->move(public_path('uploads'), $imageName1);
                $product->image1 = $imagePath1;
            }
            if ($request->image2) {
                if ($product->image2)
                    if (fileExists(public_path() . $product->image2)) {
                        File::delete(public_path() . $product->image2);
                    }
                $imageName2 = 'product_' . uniqid() . '.' . $request->image2->getClientOriginalName();
                $imagePath2 = '/uploads/' . $imageName2;
                $request->image2->move(public_path('uploads'), $imageName2);
                $product->image2 = $imagePath2;
            }
            if ($request->image3) {
                if ($product->image3)
                    if (fileExists(public_path() . $product->image3)) {
                        File::delete(public_path() . $product->image3);
                    }
                $imageName3 = 'product_' . uniqid() . '.' . $request->image3->getClientOriginalName();
                $imagePath3 = '/uploads/' . $imageName3;
                $request->image3->move(public_path('uploads'), $imageName3);
                $product->image3 = $imagePath3;
            }

            if ($request->image4) {
                if ($product->image4)
                    if (fileExists(public_path() . $product->image4)) {
                        File::delete(public_path() . $product->image4);
                    }
                $imageName4 = 'product_' . uniqid() . '.' . $request->image4->getClientOriginalName();
                $imagePath4 = '/uploads/' . $imageName4;
                $request->image4->move(public_path('uploads'), $imageName4);
                $product->image4 = $imagePath4;
            }

            if ($request->image5) {
                if ($product->image5)
                    if (fileExists(public_path() . $product->image5)) {
                        File::delete(public_path() . $product->image5);
                    }
                $imageName5 = 'product_' . uniqid() . '.' . $request->image5->getClientOriginalName();
                $imagePath5 = '/uploads/' . $imageName5;
                $request->image5->move(public_path('uploads'), $imageName5);
                $product->image5 = $imagePath5;

            }
            if ($request->days_offer != null) {

                $product->days_offer = $request->days_offer;
                $product->is_offer = true;
                $product->date_offer = Carbon::now()->addDays($request->days_offer);
                $product->price_new = $request->price_new;
                $product->save();
            }

            $category = Category::query()->where('id', $request->category_id)->first();
            if ($category) {
                $data['category_id'] = $category->id;
                $product->update($data);

                return response()->json(Product::query()->findOrFail($product->id));

            }

        });

    }

    public
    function delete($id)
    {
        $product = Product::query()->findOrFail($id);
        if ($product->image_product)
            if (fileExists(public_path() . $product->image_product)) {
                File::delete(public_path() . $product->image_product);
            }
        if ($product->image1)
            if (fileExists(public_path() . $product->image1)) {
                File::delete(public_path() . $product->image1);
            }
        if ($product->image2)
            if (fileExists(public_path() . $product->image2)) {
                File::delete(public_path() . $product->image2);
            }
        if ($product->image3)
            if (fileExists(public_path() . $product->image3)) {
                File::delete(public_path() . $product->image3);
            }
        if ($product->image4)
            if (fileExists(public_path() . $product->image4)) {
                File::delete(public_path() . $product->image4);
            }
        if ($product->image5)
            if (fileExists(public_path() . $product->image5)) {
                File::delete(public_path() . $product->image5);
            }
        $product->delete();
        return response()->json($product);

    }

    public
    function OfferDeleteProduct($id)
    {
        $product = Product::query()->where('id', $id) > first();
        $product->is_offer = 0;
        $product->days_offer = null;
        $product->date_offer = null;
        $product->price_new = null;
        $product->save();


        return response()->json($product);
    }

    public
    function getAllProductsByCategoryId($cat_id)
    {

        $products = Product::query()->where('category_id', $cat_id)->get();
        $products->map(function ($product) {
            $product ['CategoryName'] = $product->Category['name'];
        });
        return response()->json($products->makeHidden('Category'));
    }

    public
    function getAllProductsisOfferByCategoryId($cat_id)
    {
        $products = Product::query()
            ->where('category_id', $cat_id)
            ->where('is_offer', true)->get();
        return response()->json($products);
    }

    public
    function getProductById($pro_id)
    {
        $product = Product::query()->findOrFail($pro_id);
        return response()->json($product);
    }

    public
    function getAllProduct()
    {

        $product = Product::query()->get();
        return response()->json($product);
    }

    public
    function getAllProductisOffer()
    {
        $product = Product::query()->where('is_offer', true)->get();
        return response()->json($product);
    }

    public
    function getAllProductsisOfferByContentId($content_id)
    {

        $product = Product::query()->with('category')
            ->whereHas('category', function ($query) use ($content_id) {
                $query->where('content_id', $content_id);
            })
            ->where('is_offer', true)
            ->get();
        return response()->json($product->makeHidden('category'));
    }

    public
    function getAllProductsByContentId($content_id)
    {

        $product = Product::query()->with('category')->whereHas('category', function ($query) use ($content_id) {
            $query->where('content_id', $content_id);
        })
            ->get();
        return response()->json($product->makeHidden('category'));
    }

    public
    function getSearchProductsByContentIdAndIdcategory($content_id, $category_id, $name)
    {
        if ($category_id == 0) {
            $product = Product::query()->with('category')
                ->whereHas('category', function ($query) use ($content_id) {
                    $query->where('content_id', $content_id);
                })->where('is_offer', true)->where('name', 'like', '%' . $name . '%')
                ->get();
        } else {
            $category = Category::query()->findOrFail($category_id);
            $product = Product::query()->with('category')
                ->whereHas('category', function ($query) use ($content_id) {
                    $query->where('content_id', $content_id);
                })
                ->where('category_id', $category->id)->where('name', 'like', '%' . $name . '%')->get();
        }
        return response()->json($product->makeHidden('category'));
    }

    public
    function getSearchProductsByContentIdAndIdcategory2($content_id, $category_id, Request $request)
    {

        if ($category_id == 0) {
            $product = Product::query()->with('category')->whereHas('category', function ($query) use ($content_id) {
                $query->where('content_id', $content_id);
            })->where('is_offer', true)->where('name', 'like', '%' . $request->name . '%')
                ->get();
        } else {
            $category = Category::query()->findOrFail($category_id);
            $product = Product::query()->with('category')->whereHas('category', function ($query) use ($content_id) {
                $query->where('content_id', $content_id);
            })->where('category_id', $category->id)->where('name', 'like', '%' . $request->name . '%')->get();
        }
        return response()->json($product->makeHidden('category'));
    }

    public
    function getAllOffersByIdCity($city_id)
    {
        if ($city_id == 0) {
            $product = Product::query()->with('category.content.type.city')
                ->where('is_offer', true)
                ->get();

        } else {
            $product = Product::query()->with('category.content.type.city')->with('category.content.type')
                ->whereHas('category.content.type', function ($query) use ($city_id) {
                    $query->where('city_id', $city_id);
                })
                ->where('is_offer', true)
                ->get();
        }
        $product->map(function ($product) {
            $product['cityName'] = $product['category']['content']['type']['city']['name'];
            return $product;
        });
        return response()->json($product->makeHidden('category'));

    }

    public
    function getSearchOffersByIdCity($city_id, $name)
    {
        if ($city_id == 0) {
            $product = Product::query()->with('category.content.type')
                ->where('is_offer', true)
                ->where('name', 'like', '%' . $name . '%')
                ->get();

        } else {
            $product = Product::query()->with('category.content.type')->whereHas('category.content.type', function ($query) use ($city_id) {
                $query->where('city_id', $city_id);
            })
                ->where('is_offer', true)
                ->where('name', 'like', '%' . $name . '%')
                ->get();
        }
        $product->map(function ($product) {
            $product['cityName'] = $product['category']['content']['type']['city']['name'];
            return $product;
        });
        return response()->json($product->makeHidden('category'));

    }



    public
    function ContentReaded(Request $request)
    {
        if ($request->id) {
            $headers = apache_request_headers();

            $token = $request->bearerToken();
            $header = $headers['Authorization'];
            if (Str::startsWith($header, 'Bearer ')) {
                $token = Str::substr($header, 7);
            }
            $token = DB::table('api_clients')->where('api_token', $token)->first();
            $content_id = $request->id;
            if ($content_id) {
                $content_read = ContentReaded::query()->where(['content_id' => $content_id, 'token_id' => $token->id])->first();
                if ($content_read == null) {

                    ContentReaded::create([
                        'token_id' => $token->id,
                        'content_id' => $content_id,
                        'read' => 1,
                    ]);

                }
            }


            return 1;
        } else {
            return 0;
        }
    }


}

