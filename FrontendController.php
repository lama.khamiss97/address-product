<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Content;
use App\Models\Detail;
use App\Models\Type;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function home()
    {
        $cities = City::get(['id', 'name', 'imageUrl']);

        return view('frontend.home', compact('cities'));

    }


    public function getTypes($city_id)
    {
        $types = City::query()->findOrFail($city_id)->types()->get()
            ->map(function ($item) {
                $city = City::query()->where('id', $item->city_id)->first();
                $item['city_name'] = $city ? $city->name : null;
                return $item;
            });
        return view('frontend.type', compact('types', 'city_id'));
    }


    public function getcontents($type_id)
    {

        $contents = Content::query()->where('isExpired', 0)->where('is_deleted', 0)
            ->where('type_id', $type_id)
            ->with('detail', 'type')->get();
        $contents->map(function ($content) {
            $content['open'] = $this->ONorOff($content->id);
            return $content;
        });

        return view('frontend.content', compact('contents'));
    }

    public function Search(Request $request)
    {

        $contents = Content::query()
            ->get()->filter(function ($item) {
                return $item->isExpired == 0 && $item->is_deleted == 0;
            });
        $Search = $request->query('Search');

        if ($Search != null) {

            $array_url = explode('/', $request->url1);

            if (isset($array_url[1])) $page = $array_url[1];
            if (isset($array_url[2])) $item_id = (integer)$array_url[2];
            if ($page != null) {
                if ($page == 'types' && isset($item_id) && $item_id != null) {

                    $contents = Content::query()->with('type')
                        ->where('name', 'like', '%' . $Search . '%')
                        ->OrWhere('information', 'like', '%' . $Search . '%')
                        ->OrWhereHas('detail', function ($detail) use ($Search) {
                            $detail->where('location', 'like', '%' . $Search . '%')
                                ->orWhere('keywords', 'like', '%' . $Search . '%');
                        })
                        ->get()->filter(function ($item) use ($item_id) {
                            return $item->isExpired == 0 && $item->is_deleted == 0 && $item['type']['city_id'] == $item_id;
                        })->makeHidden('type');
                } elseif ($page == 'contents' && isset($item_id) && $item_id != null) {

                    $contents = Content::query()
                        ->where('name', 'like', '%' . $Search . '%')
                        ->OrWhere('information', 'like', '%' . $Search . '%')
                        ->OrWhereHas('detail', function ($detail) use ($Search) {
                            $detail->where('location', 'like', '%' . $Search . '%')
                                ->orWhere('keywords', 'like', '%' . $Search . '%');
                        })->orderBy('name')->get()->filter(function ($item) use ($item_id) {
                            return $item->type_id == $item_id && $item->isExpired == 0 && $item->is_deleted == 0;
                        });
                } elseif ($page == 'ContentSubscribe') {

                    $contents = Content::query()->with('detail')
                        ->where('name', 'like', '%' . $Search . '%')
                        ->orWhere('information', 'like', '%' . $Search . '%')
                        ->OrWhereHas('detail', function ($detail) use ($Search) {
                            $detail->where('location', 'like', '%' . $Search . '%')
                                ->orWhere('keywords', 'like', '%' . $Search . '%');
                        })->OrWhere('information', 'like', '%' . $Search . '%')
                        ->get()->filter(function ($item) {
                            return $item->subscribe == 1 && $item->isExpired == 0 && $item->is_deleted == 0
                                && $item['detail']['logo'] != null && $item['detail']['color'] != null;

                        });


                } else {

                    $contents = Content::query()
                        ->where('name', 'like', '%' . $Search . '%')
                        ->OrWhereHas('detail', function ($detail) use ($Search) {
                            $detail->where('location', 'like', '%' . $Search . '%')
                                ->orWhere('keywords', 'like', '%' . $Search . '%');
                        })
                        ->OrWhere('information', 'like', '%' . $Search . '%')
                        ->get()->filter(function ($item) {
                            return $item->isExpired == 0 && $item->is_deleted == 0;
                        });
                }

            } else {

                $contents = Content::query()
                    ->where('name', 'like', '%' . $Search . '%')
                    ->OrWhereHas('detail', function ($detail) use ($Search) {
                        $detail->where('location', 'like', '%' . $Search . '%')
                            ->orWhere('keywords', 'like', '%' . $Search . '%');
                    })
                    ->OrWhere('information', 'like', '%' . $Search . '%')
                    ->get()->filter(function ($item) {
                        return $item->isExpired == 0;
                    });
            }
        }


        $contents->map(function ($content) {
            $content['open'] = $this->ONorOff($content->id);
            return $content;
        });
        return view('frontend.content', compact('contents'));
    }

    public function ContentSubscribe()
    {

        $contents = Content::query()->with('type')
            ->whereHas('detail', function ($query) {
                $query->whereNotNull('logo');
                $query->whereNotNull('color');

            })
            ->get()->filter(function ($item) {
                return $item->subscribe == 1 && $item->isExpired == 0 && $item->is_deleted == 0;
            });
        $contents->map(function ($content) {
            $content['open'] = $this->ONorOff($content->id);
            return $content;
        });


        return view('frontend.ContentSubscribe', compact('contents'));
    }

    public function getdetails(Content $content)
    {

        if ($content->id) {
            $details = $content->detail()->get();
        } else {
            $details = Detail::cursor();
        }
        $details->map(function ($item) use ($content) {
//            $item['online']=true;
//            $diff=Carbon::now()->diffInDays()
            $item['startTime'] = date("g:i a", strtotime($item->start_time));
            $item['endTime'] = date("g:i a", strtotime($item->end_time));
            $item['open'] = $this->ONorOff($content->id);
            return $item;
        });
        $contents = Content::pluck('id', 'name');

        return view('frontend.detail', compact('contents', 'details', 'content'));
    }


    public function ONorOff($content_id)
    {
        $content = Content::query()->with('detail')->findOrFail($content_id);
        $day = Carbon::today()->format('l');

        $datetime = Carbon::now()->format('H:i:s', time());

        $detailsStratTime = $content['detail']['start_time'];
        $detailsEndTime = $content['detail']['end_time'];
        if ($datetime < $detailsEndTime && $datetime > $detailsStratTime && in_array($day, $content['detail']['days']))

            return 'open';
        else
            return 'close';


    }


}
